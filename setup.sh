#!/usr/bin/bash

setup_run=$(grep -c '# tmux session maker' "$HOME/.local/share/do-user/bash/aliases")

if [[ $setup_run -gt 0 ]]; then
	{
		echo ''
		echo '# tmux session maker'
		echo 'alias ta=tmux_session_maker'
		echo 'alias tac=tmux_session_maker ~/.config'
	} >>"$HOME/.local/share/do-user/bash/aliases"
fi
