#!/usr/bin/bash

function install() {
	printf "%s\n" "Installing tmux_session_maker"
	cp -f -p ./src/main.sh "$HOME"/.local/bin/tmux_session_maker
}

if [ "${BASH_SOURCE[0]}" -ef "$0" ]; then
	printf "%s\n" "Script is being executed!"
	install
else
	printf "%s\n" "Script is being sourced!"
fi
