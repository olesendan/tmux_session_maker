#!/usr/bin/bash
shopt -s inherit_errexit nullglob

function error_settings() {
    local error_setting="${1}"
    case "$error_setting" in
    on)
        set -o errexit
        set -o pipefail
        set -o errtrace
        trap '__error_handler__ $? $LINENO' ERR
        ;;
    off)
        set +o errexit
        set +o pipefail
        set +o errtrace
        trap - ERR
        ;;
    esac
}

function __error_handler__() {
    trap 'echo >&2 "Error - exited with status $? at line $LINENO:";
		pr -tn $0 | tail -n+$((LINENO - 3)) | head -n7' ERR
    error_settings "off"
}

function set_session_path() {
    local user_input="${1:-""}"
    if [ -n "$user_input" ]; then
        case "$user_input" in
        "_scratch")
            selected=$(echo "$PWD")
            ;;
        "_zettelkasten")
            selected="$HOME"/zettelkasten
            ;;
        *)
            selected="$user_input"
            ;;
        esac
    else
        selected=$(
            find \
                "$HOME"/codebases/ \
                -maxdepth 7 \
                -not \( -path "*/.*" \
                -o -path "*/.*/*" \
                -o -path "*/__*" \
                -o -path "*/__*/*" \
                -o -path "*/alembic" \
                -o -path "*/alembic/*" \
                -o -path "*/config_files" \
                -o -path "*/config_files/*" \
                -o -path "*/invoice_tracking" \
                -o -path "*/invoice_tracking/*" \
                -o -path "*/misc" \
                -o -path "*/misc/*" \
                -o -path "*/src" \
                -o -path "*/src/*" \
                -o -path "*/template*" \
                -o -path "*/template*/*" \
                -o -path "*/test" \
                -o -path "*/test/*" \
                -o -path "*/tests" \
                -o -path "*/tests/*" \
                \) \
                -type d |
                sort |
                fzf --reverse --preview 'ls {}'
        )
    fi
    do_tmux_session_path="$selected"
}

function set_session_name() {
    local user_input=$1
    local base_name
    local session_path=$2
    local session_name
    local username
    local hostname
    username="$(whoami)"
    hostname="$(echo "$NAME")"
    # echo "$username"
    # echo "$hostname"
    if [[ -z "$user_input" ]] || [[ -z "$session_path" ]]; then
        printf "%s\n" "Parameters are not set correctly"
        return 1
    fi
    # printf "%s\n" "$session_path"
    case "$user_input" in
    "_scratch")
        base_name="_scratch_$(tmux display -p '#S')"
        ;;
    *)
        base_name=$(basename "$session_path" | tr . _)
        ;;
    esac
    if [[ $base_name == "$username" ]]; then
        session_name="$base_name"_"$hostname"
    else
        session_name="$base_name"
    fi
    do_tmux_session_name="$session_name"
}

function create_tmux_session() {
    local session_name=$1
    local session_path=$2
    if [ -z "$session_name" ] || [ -z "$session_path" ]; then
        printf "%s\n" "Parameters are not set correctly"
        return 1
    fi
    if tmux has-session -t "$session_name" 2>/dev/null; then
        printf "%s\n" "Session already exists"
    else
        printf "%s\n" "Creating session"
        session_id="$(tmux new-session -dP -s "$session_name" -c "$session_path" -F '#{session_id}')"
        if echo "$do_tmux_session_name" | grep "_scratch_"; then
            tmux set-option -s -t "$session_id" key-table scratch
            tmux set-option -s -t "$session_id" mouse on
            # tmux set-option -s -t "$session_id" status off
            # tmux set-option -s -t "$session_id" prefix None
            tmux set-environment -t "$session_name" AZURE_THREAD "$session_name"
            tmux set-environment -t "$session_name" AZURE_ROLE "I want you to act as a bash, python, lua and golang programming mentor, helping me with my coding tasks and expanding my knowledge. Share tips, tricks, and guiding principles to improve my skills and make my code more efficient. Explain common errors or challenges I might encounter along the way and provide examples or exercises to practice different programming concepts. Help me understand best practices and design patterns in bash, python, lua and golang and encourage me to explore advanced topics such as working with libraries and frameworks."
            tmux new-window -S -d -n "chatgpt" \; send-keys -t "chatgpt" 'chatgpt --interactive' Enter
        fi
    fi
}

function switch_tmux_session() {
    local session_name=$1
    local session_path=$2
    if [ -z "$session_name" ] || [ -z "$session_path" ]; then
        printf "%s\n" "parameter is not set"
        return 1
    fi
    if [ -z "$TMUX" ]; then
        printf "%s\n" "Attaching to $session_name"
        tmux attach -t "$session_name"
    else
        printf "%s\n" "Switching to $session_name"
        if echo "$session_name" | grep "_scratch_"; then
            printf "%s\n" "_scratch"
            tmux attach -t "$session_name"
        else
            printf "%s\n" "full screen"
            tmux switch-client -t "$session_name"
        fi
    fi
}

function main() {
    printf "%s\n" "main function"
    declare user_input
    user_input="$1"
    declare do_tmux_session_name
    declare do_tmux_session_path
    set_session_path "$user_input"
    set_session_name "$user_input" "$do_tmux_session_path"
    create_tmux_session "$do_tmux_session_name" "$do_tmux_session_path"
    switch_tmux_session "$do_tmux_session_name" "$do_tmux_session_path"
}

if [[ "${BASH_SOURCE[0]}" -ef "$0" ]]; then
    printf "%s\n" "script is being executed!"
    user_input="${1:-""}"
    error_settings "on"
    main "$user_input"
    error_settings "off"
else
    printf "%s\n" "script is being sourced!"
fi
